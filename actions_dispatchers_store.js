import _ from 'lodash'
import logger from '#logger'

class ActionsDispatchersStore {
    constructor(params) {
        _.assign(this, params)
        this.data = []
        this.log = logger.child({ class: this.constructor.name })
    }

    get(filter) {
        return _.find(this.data, filter)?.dispatcher
    }

    getIds(filter) {
        return _.map(_.filter(this.data, filter), ({ id }) => id)
    }

    add(id, dispatcher) {
        if (this.get({ id })) {
            throw 'Add dispatcher only with unique id is allowed'
        }

        const { transceiver } = dispatcher
        transceiver.addEventListener('close', () => {
            if (this.closeMarkFlag) {
                this.markClose(id)
            } else {
                this.remove(id)
            }
        })

        this.data.push({ id, dispatcher })
        this.log.info({ id }, 'Dispatcher added')
    }

    remove(id) {
        if (_.remove(this.data, { id })) {
            this.log.info({ id }, 'Dispatcher removed')
        }
    }

    markClose(id) {
        const datum = _.find(this.data, { id })
        if (datum) {
            datum.closed = true
            this.log.info({ id }, 'Dispatcher marked as closed')
        }
    }
}

export default ActionsDispatchersStore
